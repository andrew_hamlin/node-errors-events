var logger = require('logfmt'),
    debug = require('debug')('app'),
    extend = require('extend'),
    EventEmitter = require('events').EventEmitter;

var connections = require('./connections');
var messages = require('./controllers/messages');

function App(config) {
    EventEmitter.call(this); // extends EventEmitter

    var self = this;

    this.on('newListener', function (listener) {
        debug('new listener: ' + listener);
    });

    this.connections = connections(config.mongo_url)
        .once('ready', self.onReady.bind(self))
        .once('lost', self.onLost.bind(self));

}

App.prototype = Object.create(EventEmitter.prototype);


module.exports = function createApp(config) {
    return new App(config);
};

extend(App.prototype, {
    // getController: function() {
    //     debug('get controller');
    //     return this._controller || {};
    // },
    onReady: function () {
        this.controller = messages(this);
        logger.log({ type: 'info', msg: 'app.ready' });
        this.emit('ready');
    },
    
    onLost: function () {
        logger.log({ type: 'info', msg: 'app.lost' });
        this.emit('lost');
    }
});
