module.exports = {

    mongo_url: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/dev',
    rabbit_url: process.env.CLOUDAMQP_URL || 'amqp://localhost',
    port: int(process.env.PORT) || 3000,
    
    concurrency: int(process.env.CONCURRENCY) || 1,
    debugging: (process.env.NODE_ENV === 'development'),
    thrifty: bool(process.env.THRIFTY) || false,
    verbose: bool(process.env.VERBOSE) || false,
    view_cache: bool(process.env.VIEW_CACHE) || false

};


function bool (str) {
    if (str === undefined) return false;
    return str.toLowerCase() === 'true';
}

function int (str) {
    if (!str) return 0;
    return parseInt(str, 10);
}

function float (str) {
    if (!str) return 0;
    return parseFloat(str, 10);
}
