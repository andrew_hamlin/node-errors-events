var mongoose = require('mongoose'),
    debug = require('debug')('app:connections'),
    logger = require('logfmt'),
    EventEmitter = require('events').EventEmitter;

function Connector(mongoUrl) {
    EventEmitter.call(this);

    var self = this;
    var readyCount = 0;
    var FIRE_WHEN_READY = 1;

    this.db = mongoose.createConnection(mongoUrl)
        .on('connected', function () {
            logger.log({ type: 'info', msg: 'connected', service: 'mongodb' });
            ready();
        })
        .on('error', function(error) {
            logger.log({ type: 'error', msg: error, service: 'mongodb' });
        })
        .on('close', function () {
            logger.log({ type: 'info', msg: 'closed', service: 'mongodb' });
        })
        .on('disconnected', function () {
            logger.log({ type: 'error', msg: 'disconnected', service: 'mongodb' });
            lost();
        });

    function ready() {
        if (++readyCount === FIRE_WHEN_READY) {
            debug('connections.ready');
            self.emit('ready');
        }
    }
    function lost() {
        self.emit('lost');
    }
}

Connector.prototype = Object.create(EventEmitter.prototype);

module.exports = function createConnections(mongoUrl) {
    return new Connector(mongoUrl);
};
