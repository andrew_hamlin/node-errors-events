var debug = require('debug')('app:controllers'),
    messageModel = require('../models/message');

module.exports = function createMessageController(app) {
    var Message = messageModel(app.connections.db);
    debug('createMessageController');

    return {
        getMessages: function (req, res, next) {
            debug('getMessages from ' + this.Message);
            Message.find({}, function (err, messages) {
                if (err) {
                    debug('error ' + err);
                    return next(err);
                }
                debug('found messages: ' + messages);
                res.json(messages);
            });
        },
        createMessage: function (req, res, next) {
            Message.create(req.body, function (err, message) {
                if (err) {
                    return next(err);
                }
                debug('create message: ' + message);
                res.json(message);
            });
        },
        getMessageById: function (req, res, next) {
            Message.findOne({ _id: req.params.id }, function (err, message) {
                if (err) {
                    return next(err);
                }
                debug('found message: ' + message);
                res.json(message);
            });
        },
        updateMessage: function (req, res, next) {
            Message.findOneAndUpdate({ _id: req.params.id }, req.body, function (err, message) {
                if (err) {
                    return next(err);
                }
                debug('update message: ' + message);
                res.json(message);
            });
        },
        deleteMessage: function (req, res, next) {
            Message.findOneAndRemove({ _id: req.params.id }, function (err, message) {
                if (err) {
                    return next(err);
                }
                debug('removed message: ' + message);
                res.json(message);
            });
        }
    };
};
