var logger = require('logfmt'),
    debug = require('debug')('web:errors'),
    util = require('util');

function PathNotFoundError (message) {
    Error.call(this);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
}

util.inherits(PathNotFoundError, Error);

module.exports = function Controller(verbose, debugging) {
    return {
        pathNotFound: pathNotFound,
        log: logError,
        json: jsonError,
        html: htmlError
    };

    function pathNotFound (req, res, next) {
        debug('pathNotFound');
        var err = new PathNotFoundError('Path not found ' + req.path);
        err.status = 404;
        next(err);
    }

    function logError (err, req, res, next) {
        if (err.status === 404 && !verbose) {
            return next(err);
        }
        // XXX update to check development mode
        debug('logError');
        logger.log({
            type: 'error',
            message: err.message || 'middleware error',
            err: err.stack || 'No stack trace available'
        });
        return next(err);
    }

    function jsonError(err, req, res, next) {
        debug('jsonError');
//        if (req.path.slice(-5) !== '.json') return next(err);
        if (!req.accepts('application/json')) return next(err);
        debug('handling json error ' + req.accepts('json'));
        return res
               .status(err.status || 500)
               .json({
                   message: err.message || 'Oops! Something went wrong.',
                   error: (debugging ? err : {})
               });
    }

    function htmlError(err, req, res, next) {
        debug('htmlError');
//        if (!req.accepts('html')) return next(err);
        debug('handling html error ' + req.accepts('html'));
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: (debugging ? err : {})
        });
    }
};
