var debug = require('debug')('app:models'),
    mongoose = require('mongoose');

module.exports = function createMessageModel(connection) {
    debug('createMessageModel');

    var MessageSchema = mongoose.Schema({
        text: { type: String },
        created_at: { type: Date, default: Date.now },
        created_by: mongoose.Schema.Types.ObjectId,
        updated_at: { type: Date }
    });

    // mongoose is a bit confusing when it comes to firing events
    // on documents versus queries. The pre('save') event is fired
    // when you create a document (Model.create) or when you call
    // document.save but when you use Model.findOneAndUpdate or just
    // update the event is fired from the Query. So, you have to
    // manually call findOneAndUpdate or update.
    // See https://github.com/Automattic/mongoose/issues/964
    MessageSchema.pre('findOneAndUpdate', function (next) {
        debug('setting updated_at');
        this.findOneAndUpdate({}, { $set: { updated_at: Date.now() }});
        next();
    });

    return connection.model('Message', MessageSchema);
};
