var debug = require('debug')('web:pages');


module.exports = function loadPages(web) {
    debug('loadPages called');

    web
    .get('/', index);

    function index(req, res) {
        res.render('index');
    }
};
