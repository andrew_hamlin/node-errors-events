var express = require('express');
// debug = require('debug')('app:routers');

module.exports = function createV1Api(app){
    var router = express.Router();

    router.get('/messages', app.controller.getMessages);
    router.get('/messages/:id', app.controller.getMessageById);
    router.post('/messages', app.controller.createMessage);
    router.put('/messages/:id', app.controller.updateMessage);
    router.delete('/messages/:id', app.controller.deleteMessage);

    return router;
};
