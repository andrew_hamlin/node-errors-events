var logger = require('logfmt');
var debug = require('debug')('server');

var config = require('./config');
var app = require('./app');
var web = require('./web');

function start() {
    logger.log({
        type: 'info',
        msg: 'starting server',
        concurrency: config.concurrency,
        debugging: config.debugging,
        thrifty: config.thrifty,
        verbose: config.verbose,
        view_cache: config.view_cache
    });

    var instance = app(config)
        .on('ready', startServer)
        .on('lost', abortServer);

    //instance.onReady();

    function startServer () {
        debug('constructing web(app, config)');
        var http = web(instance, config);
        var server = http.listen(config.port);
        debug('server listen called');
        server.on('error', onError);
        server.on('listening', onListening);
        process.on('SIGTERM', onShutdown);
        process.on('SIGINT', onShutdown);

        instance
            .removeListener('lost', abortServer)
            .on('lost', onShutdown);
        
        function onListening () {
            logger.log({ type: 'info', msg: 'listening', port: server.address().port });
        }

        function onError (err) {
            logger.log({ type: 'error', msg: 'Error on server', error: err });
        }

        function onShutdown () {
            logger.log({ type: 'info', msg: 'shutting down' });
            server.close(function () {
                logger.log({ type: 'info', msg: 'exited' });
                process.exit();
            });
        }
    }
    function abortServer () {
        logger.log({ type: 'info', msg: 'aborting', abort: true });
        process.exit();
    }
}

start();
