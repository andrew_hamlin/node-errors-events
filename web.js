var logger = require('morgan'),
    path = require('path'),
    express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser');

var errors = require('./errors');
var pages = require('./pages');
//var testmodel = require('./testmodel/router');
var v1api = require('./routers/v1');

module.exports = function Web(app, config) {

    var web = express();
    var errs = errors(config.verbose, config.debugging);


    web
        .set('views', path.join(__dirname, 'views'))
        .set('view engine', 'ejs')
        .set('view cache', config.view_cache);

    web
        .use(logger('dev'))
        .use(bodyParser.json())
        .use(bodyParser.urlencoded({ extended: true }))
        .use(cookieParser());

    // web pages
    pages(web);

    // routers
    web
    //.use('/api', testmodel(app))
        .use('/api', v1api(app));

    // error handling
    web
        .use(errs.pathNotFound)
        .use(errs.log)
        .use(errs.json)
        .use(errs.html)
    ;
    return web;
};
